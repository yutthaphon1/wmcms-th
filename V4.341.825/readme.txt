# Weimeng CMS (WMCMS) ระบบจัดการเนื้อหาสำเร็จรูป
  WMCMS เป็นระบบจัดการเนื้อหาสำเร็จรูป ก่อตั้งขั้นในเดือนมกราคมปีพุทธศักราช 2555 เป็นระบบที่ตั้งอยู่บนพื้นฐานของ PHP + MySQL ที่มุ่งในการพัฒนาให้ใช้ได้ฟรีและโอเพนซอร์ส ตัวระบบเน้นไปที่การจัดการนิยายเป็นหลัก โดยตัวระบบถูกสร้างขึ้นเพื่ออำนวยความสะดวกแก่ผู้ดูแลเว็บไซต์ ซึ่งสามารถจัดการส่วนต่าง ๆ ได้อย่างง่ายดายโดยที่ไม่ต้องทำระบบด้วยตนเอง ในส่วนของนักเขียนหรือนักแปลเองก็สามารถลงผลงานด้วยตนเอง รวมไปถึงหารายได้จากการเขียนหรือแปลจากผลงานได้โดยตรงด้วยระบบ VIP ที่สามารถซื้อเป็นบท เป็นเล่ม หรือทั้งเรื่องได้

## หน้าเว็บอย่างเป็นทางการ >> [WeimengCMS](http://www.weimengcms.com)

## โมดูล
- โมดูลบทความ
- โมดูลเกี่ยวกับ
- โมดูลนิยาย
- โมดูลคุณลักษณะ
- โมดูลหน้าเดี่ยว
- โมดูลรูปภาพ
- โมดูลเว็บเพื่อนบ้าน
- โมดูลผู้ใช้
- โมดูลเว็บบอร์ด
- โมดูลแอปพลิเคชั่น
- โมดูลค้นหา

## การนำไปใช้
- เว็บบล็อกส่วนตัว
- เว็บองค์กร
- เว็บไซต์แสดงรูปภาพ
- เว็บนิยาย
- เว็บการ์ตูน
- เว็บวีดีโอ
- เว็บหนัง
- เว็บอนิเมะ
- อื่น ๆ

# อัปเดทจาก V4.340.815 เป็น V4.341.825

## รายการอัพเดท
### เพิ่มใหม่ [ 1 รายการ ]
- เพิ่ม action คำร้องปลั๊กอินเพื่อแนะนำไฟล์ hook ก่อนและหลังที่กำหนดโดยปลั๊กอิน

## รายการแก้ไข
### แก้ไข [ 10 รายการ ]
- เพิ่มวิธีการล้างที่ตั้งโดยใช้ ',' เพื่อนำออก
- แก้ไขการตรวจสอบเวลาค้นหาผิดพลาด
- เพิ่มการตรวจจับเวลารีเฟรชในหาน้าค้นหา
- แก้ไขปัญหาหน้าต่างเตือนว่าไม่มีอยู่ของการชำระเงินผ่าน Alipay เนื่องจากสภาพแวดล้อมที่ใหม่กว่า php7.2
- แก้ไขปัญหาหน้าต่างเตือนว่าไม่มีอยู่ของการชำระเงินผ่าน WeChat เนื่องจากสภาพแวดล้อมที่ใหม่กว่า php7.2
- แก้ไขลูปคลาสชำระเงินผ่าน Alipay ที่ไม่รองรับ php7.2 เปลี่ยนเป็น foreach
- แก้ไขปัญหาบอรืดไม่สามารถดูหน้าได้อย่างถูกต้อง
- เพิ่มประสิทธิภาพป้ายกำกับ wmsql ให้รองรับป้ายกำกับสาธารณะ
- เพิ่มประเภทโค้ดคำเชิญในส่วนจัดการพื้นหลัง
- แก้ไขโมเดล card เพื่อแก้ไขสถานะการ์ดให้ตรงตามสถานการณ์

## แปลไทยโดย [SubMaRk](https://naynum.engineer)
### งานแปลนี้ไม่ตรงตามความหมาย 100% เป็นงานแปลเท่าที่สามารถแปลได้ หากพบคำผิด สามารถ Fork หรือ Clone git นี้ไปแก้ไขได้เลยครับ